import {ConnectionType} from "./connections";
import * as schedule from "node-schedule";
import {Job} from "node-schedule";
import {defaultExecOptions, execute} from "./executor";
import {
    EXEC_DRY_RUN,
    REBOOT_COMMAND,
    REBOOT_COMMAND_SSH,
    RETRY_INTERVAL,
    VPN_RESTART_COMMAND,
    VPN_RESTART_COMMAND_SSH
} from "./constants";
import {Observable} from "rxjs";
import moment = require("moment");

const logger = new console.Console(process.stdout, process.stderr);
require('console-stamp')(logger, {
    metadata: '[HOST]'
});

const JOB_REBOOT = 'reboot';
const JOB_VPN_RESTART = 'vpn restart';

export class Host {

    private rebootReasons = [];
    private jobs: Map<string, Job> = new Map<string, Job>();

    public scheduleReboot(reason: ConnectionType, msOffset: number = 0) {
        const index = this.rebootReasons.indexOf(reason);
        if (index === -1) {
            this.rebootReasons.push(reason);
        }
        logger.log(`trying to schedule reboot (reason: ${ConnectionType[reason]}), current reboot reasons: `, this.rebootReasons.map(it => ConnectionType[it]));

        const storedJob = this.jobs.get(JOB_REBOOT);
        const nextInvocation = storedJob && storedJob.nextInvocation();
        if (nextInvocation == null) {
            this.retryableExecJob(JOB_REBOOT, msOffset, (): Observable<any> => {
                logger.log('rebooting');
                return execute(REBOOT_COMMAND_SSH, REBOOT_COMMAND, [], {
                    ...defaultExecOptions,
                    dryRun: EXEC_DRY_RUN
                });
            });
        } else {
            logger.log(`reboot already scheduled for ${nextInvocation}`);
        }
    }

    public cancelReboot(reason: ConnectionType) {
        const index = this.rebootReasons.indexOf(reason);
        if (index > -1) {
            this.rebootReasons.splice(index, 1);
        }
        logger.log(`trying to cancel reboot (reason: ${ConnectionType[reason]}), current reboot reasons: `, this.rebootReasons.map(it => ConnectionType[it]));
        const storedJob = this.jobs.get(JOB_REBOOT) as any;
        if ((storedJob && storedJob.nextInvocation()) && this.rebootReasons.length === 0) {
            storedJob.cancel();
            logger.log('reboot cancelled');
        }
    }

    public scheduleVpnRestart(msOffset: number = 0,
                              successCallback?: (next) => void) {
        logger.log(`scheduling vpn restart in ${moment.duration(msOffset, 'milliseconds').asSeconds()} seconds`);
        this.retryableExecJob(JOB_VPN_RESTART, msOffset, (): Observable<any> => {
            logger.log('restarting vpn');
            return execute(VPN_RESTART_COMMAND_SSH, VPN_RESTART_COMMAND, [], {
                ...defaultExecOptions,
                dryRun: EXEC_DRY_RUN
            });
        }, successCallback);
    }

    public cancelVpnRestart() {
        const storedJob = this.jobs.get(JOB_VPN_RESTART) as any;
        if (storedJob && storedJob.nextInvocation()) {
            storedJob.cancel();
            logger.log('vpn restart cancelled');
        }
    }

    private retryableExecJob(jobName,
                             msOffset,
                             exec: () => Observable<any>,
                             successCallback: (next) => void = () => logger.log(`${jobName} succeeded`)) {
        const jobSchedule = new Date(Date.now() + 10 + msOffset);
        const job = schedule.scheduleJob(jobName, jobSchedule, () => {
            exec().subscribe(successCallback,
                err => {
                    const storedJob = this.jobs.get(jobName) as any;
                    storedJob.retryCount = storedJob.retryCount !== undefined ? storedJob.retryCount + 1 : 1;
                    logger.error(`${jobName} failed, retrying for ${storedJob.retryCount}. time`, err);
                    storedJob.reschedule(new Date(Date.now() + (RETRY_INTERVAL * 1000)).getTime());
                });
        });
        this.jobs.set(jobName, job);
        logger.log(`${jobName} scheduled for ${jobSchedule} (${moment.duration(msOffset, 'milliseconds').asSeconds()} seconds)`);
    }

}
