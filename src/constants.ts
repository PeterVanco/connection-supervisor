import * as config from 'config';

console.log('Using following configuration sources:');
console.log(config.util.getConfigSources().map(src => src.name));
console.log('Using following configuration:');
console.log(JSON.stringify(config.util.toObject(), null, 2));

export const PORT: number = config.get('port');
export const LOG_CHECKS: number = config.get('log_checks');
export const CHECK_INTERVAL: number = config.get('check_interval');
export const RETRY_INTERVAL: number = config.get('retry_interval');
export const REBOOT_COMMAND_SSH: string = config.get('reboot_command.ssh');
export const REBOOT_COMMAND: string = config.get('reboot_command.command');

export const ETH_STATIC_IP: string = config.get('eth.static_ip');
export const ETH_NETWORK_PREFIX: string = config.get('eth.network_prefix');

export const VPN_NETWORK_PREFIX: string = config.get('vpn.network_prefix');
export const VPN_STATIC_IP: string = config.get('vpn.static_ip');
export const VPN_HOST_IP: string = config.get('vpn.host_ip');
export const VPN_RESTART_COMMAND_SSH: string = config.get('vpn.restart_command.ssh');
export const VPN_RESTART_DELAY: string = config.get('vpn.restart_delay');
export const VPN_RESTART_DELAY_VARIANCE: string = config.get('vpn.restart_delay_variance');
export const VPN_RESTART_COMMAND: string = config.get('vpn.restart_command.command');

export const PING_HOST: string = config.get('internet.ping_host') || 'www.google.sk';
export const NO_INTERNET_REBOOT_MINUTES: number = config.get('internet.reboot_delay');
export const NO_VPN_REBOOT_MINUTES: number = config.get('internet.reboot_delay');

export const SSH_HOST: string = config.get('ssh.host');
export const SSH_PORT: number = config.get('ssh.port');
export const SSH_USERNAME: string = config.get('ssh.username');
export const SSH_PASSWORD: string = config.has('ssh.password') && config.get('ssh.password');
export const SSH_PRIVATE_KEY: string = config.has('ssh.privateKey') && config.get('ssh.privateKey');
export const EXEC_DRY_RUN: boolean = Boolean(config.get('exec.dry_run'));


