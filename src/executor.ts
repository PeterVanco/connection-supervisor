import {from, Observable} from "rxjs";
import {exhaustMap, finalize} from "rxjs/operators";
import * as SSH from 'node-ssh';
import {SSH_HOST, SSH_PASSWORD, SSH_PORT, SSH_PRIVATE_KEY, SSH_USERNAME} from "./constants";

const {exec} = require('child_process');

const logger = new console.Console(process.stdout, process.stderr);
require('console-stamp')(logger, {
    metadata: '[EXECUTOR]'
});

const ssh = new SSH();

export const defaultExecOptions = {
    dryRun: false,
    onStdout(chunk: any) {
        logger.info(chunk.toString());
    },
    onStderr(chunk: any) {
        logger.error(chunk.toString());
    },
};

function getSshConnectionOptions() {
    const options = {
        host: SSH_HOST,
        port: SSH_PORT,
        username: SSH_USERNAME
    };
    if (SSH_PRIVATE_KEY) {
        return {
            ...options,
            privateKey: SSH_PRIVATE_KEY,
        }
    }
    return {
        ...options,
        password: SSH_PASSWORD,
        tryKeyboard: true,
        onKeyboardInteractive: (name, instructions, instructionsLang, prompts, finish) => {
            console.log(prompts);
            if (prompts.length > 0 && prompts[0].prompt.toLowerCase().includes('password')) {
                finish([SSH_PASSWORD])
            }
        }
    }
}

export function execute(ssh, command, params = [], options = defaultExecOptions): Observable<any> {
    return ssh ? execSsh(command, params, options) : execLocally(command, params, options);
}

function execLocally(command, params = [], options = defaultExecOptions): Observable<any> {
    const {dryRun, ...execOptions} = options as any;
    return new Observable(o$ => {
        exec(!!dryRun ? `echo ${command}` : command, (err, stdout, stderr) => {
            if (err) {
                logger.error(`exec error: ${err}`);
                o$.error(err);
                o$.complete();
                return;
            }
            if (stdout) {
                execOptions.onStdout(stdout);
            }
            if (stderr) {
                execOptions.onStderr(stderr);
            }
            o$.next({stdout, stderr});
            o$.complete();
        });
    });
}

function execSsh(command, params = [], options = defaultExecOptions): Observable<any> {
    const {dryRun, ...execOptions} = options as any;
    return from(ssh.connect(getSshConnectionOptions())).pipe(
        exhaustMap(() => {
            logger.log(`executing (dryRun: ${!!dryRun}): ${command}`);
            return from(ssh.exec(!!dryRun ? `echo ${command}` : command, params, execOptions));
        }),
        finalize(() => {
            try {
                ssh.dispose();
            } catch (err) {
                logger.error('could not dispose ssh connection', err)
            }
        })
    );
}