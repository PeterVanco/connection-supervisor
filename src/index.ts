import * as express from 'express';
import {interval} from 'rxjs';
import 'rxjs/add/operator/startWith';
import {distinctUntilChanged, pairwise, startWith, takeWhile} from "rxjs/operators";

import {
    CHECK_INTERVAL,
    NO_INTERNET_REBOOT_MINUTES,
    NO_VPN_REBOOT_MINUTES,
    PORT,
    VPN_RESTART_DELAY,
    VPN_RESTART_DELAY_VARIANCE
} from './constants';
import './checks';
import {connections, ConnectionStatus, ConnectionStatusWrapper, ConnectionType} from "./connections";
import {ICheck} from "./checks/check";
import {Host} from './host';
import {NetworkInterface} from "./network-interface";
import * as schedule from "node-schedule";
import moment = require("moment");

const logger = new console.Console(process.stdout, process.stderr);
require('console-stamp')(logger, {
    metadata: '[MAIN]'
});

const app = express();
const host = new Host();

const hooks = new Map<ConnectionType, Map<ConnectionStatus, Function>>();

const internetHooks = new Map<ConnectionStatus, (wrapper: ConnectionStatusWrapper) => any>();
internetHooks.set(ConnectionStatus.DOWN, () => {
    const offset = moment.duration(NO_INTERNET_REBOOT_MINUTES, 'seconds').asMilliseconds();
    host.scheduleReboot(ConnectionType.INTERNET, offset);
});
internetHooks.set(ConnectionStatus.UP, () => host.cancelReboot(ConnectionType.INTERNET));
hooks.set(ConnectionType.INTERNET, internetHooks);

const ethHooks = new Map<ConnectionStatus, (wrapper: ConnectionStatusWrapper) => any>();
ethHooks.set(ConnectionStatus.DOWN, () => {
    const offset = moment.duration(NO_INTERNET_REBOOT_MINUTES, 'seconds').asMilliseconds();
    host.scheduleReboot(ConnectionType.ETH, offset);
});
ethHooks.set(ConnectionStatus.UP, () => host.cancelReboot(ConnectionType.ETH));
hooks.set(ConnectionType.ETH, ethHooks);

const vpnHooks = new Map<ConnectionStatus, (wrapper: ConnectionStatusWrapper) => any>();
vpnHooks.set(ConnectionStatus.DOWN, check => {
    const vpnOffset = Math.floor(Math.random() * moment.duration(VPN_RESTART_DELAY_VARIANCE, 'seconds').add(VPN_RESTART_DELAY, 'seconds').asMilliseconds());
    host.scheduleVpnRestart(vpnOffset, out => {
        logger.log('vpn restart succeeded, resetting status');
        return check.status = ConnectionStatus.UNKNOWN;
    });
    const rebootOffset = moment.duration(NO_VPN_REBOOT_MINUTES, 'seconds').asMilliseconds();
    host.scheduleReboot(ConnectionType.VPN, rebootOffset);
});
vpnHooks.set(ConnectionStatus.UP, () => {
    host.cancelVpnRestart();
    host.cancelReboot(ConnectionType.VPN);
});
hooks.set(ConnectionType.VPN, vpnHooks);

app.get('/status', (req, res) => {
    res.writeHead(200, {'Content-Type': 'application/json'});
    const jsonRes = Array.from(connections).reduce((obj, [key, value]) => {
        obj[ConnectionType[key]] = value.asObject();
        return obj;
    }, {});
    res.end(JSON.stringify(jsonRes, null, 2));
});

app.listen(PORT, () => {
    logger.log(`Server is running in http://localhost:${PORT}`);

    NetworkInterface.printAll();

    const source = interval(CHECK_INTERVAL * 1000).startWith(0);

    ICheck.getImplementations().forEach(ctor => {
        const check = new ctor();

        check.getCheck(source)
            .subscribe((res: ConnectionStatus) => {
                connections.get(check.getCheckType()).status = res;
            });

        connections.get(check.getCheckType()).observableStatus().pipe(
            takeWhile((value, index) => (value !== null) || (index === 0)),
            startWith(ConnectionStatus[ConnectionStatus.UNKNOWN]),
            distinctUntilChanged(),
            pairwise(),
        ).subscribe(([previous, current]) => {
            logger.log(`${ConnectionType[check.getCheckType()]} changed status from ${previous} to ${current}`);
            const hookGroup = hooks.get(check.getCheckType());
            if (hookGroup) {
                const hook = hookGroup.get(ConnectionStatus[current]);
                if (hook) {
                    logger.log(`executing hook ${ConnectionType[check.getCheckType()]}::${current}`);
                    hook(connections.get(check.getCheckType()));
                }
            }
        });
    });

});

schedule.scheduleJob('supervisor', '*/15 * * * *', () => {
    logger.log('alive');
});
