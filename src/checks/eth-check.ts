import {catchError, switchMap, tap} from "rxjs/operators";
import {Observable, of} from "rxjs";

import {ConnectionStatus, ConnectionType} from "../connections";
import {ETH_NETWORK_PREFIX, ETH_STATIC_IP} from "../constants";
import {NetworkInterface} from "../network-interface";
import {ICheck, logConditionally} from "./check";

const logger = new console.Console(process.stdout, process.stderr);
require('console-stamp')(logger, {
    metadata: '[' + ConnectionType[ConnectionType.ETH] + ']',
    colors: {
        metadata: require('chalk').green.bold
    }
});

@ICheck.register
export class EthCheck {

    private eth = new NetworkInterface(ETH_NETWORK_PREFIX);

    public getCheckType() {
        return ConnectionType.ETH;
    }

    public getCheck(source: Observable<any>): Observable<ConnectionStatus> {
        logger.log(`starting ${ConnectionType[this.getCheckType()]} check with parameters: ETH_NETWORK_PREFIX=${ETH_NETWORK_PREFIX}, ETH_STATIC_IP=${ETH_STATIC_IP}`);
        return source.pipe(
            tap(() => logConditionally(logger, 'checking')),
            switchMap(() => {
                const ethIp = this.eth.getIp();
                const hasEth = ethIp == ETH_STATIC_IP;
                if (hasEth) {
                    logConditionally(logger, 'connected');
                } else {
                    logger.warn(`not connected (expected ${ETH_STATIC_IP}, got ${ethIp})`);
                }
                return of(hasEth ? ConnectionStatus.UP : ConnectionStatus.DOWN);
            }),
            catchError(err => {
                console.error('check failed', err);
                return of(ConnectionStatus.UNKNOWN);
            })
        );
    }

}
