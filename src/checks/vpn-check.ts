import {catchError, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {from, Observable, of} from "rxjs";
import * as PING from "ping";

import {ConnectionStatus, ConnectionType} from "../connections";
import {VPN_HOST_IP, VPN_NETWORK_PREFIX, VPN_STATIC_IP} from "../constants";
import {ICheck, logConditionally} from "./check";
import {NetworkInterface} from "../network-interface";

const logger = new console.Console(process.stdout, process.stderr);
require('console-stamp')(logger, {
    metadata: '[' + ConnectionType[ConnectionType.VPN] + ']',
    colors: {
        metadata: require('chalk').magenta.bold
    }
});


@ICheck.register
export class VpnCheck {

    private vpn = new NetworkInterface(VPN_NETWORK_PREFIX);

    public getCheckType() {
        return ConnectionType.VPN;
    }

    public getCheck(source: Observable<any>): Observable<ConnectionStatus> {
        logger.log(`starting ${ConnectionType[this.getCheckType()]} check with parameters: VPN_NETWORK_PREFIX=${VPN_NETWORK_PREFIX}, VPN_STATIC_IP=${VPN_STATIC_IP}, VPN_HOST_IP=${VPN_HOST_IP}`);
        return source.pipe(
            tap(() => logConditionally(logger, 'checking')),
            switchMap(() => {
                const vpnIp = this.vpn.getIp();
                const hasVpn = vpnIp == VPN_STATIC_IP;
                if (hasVpn) {
                    logConditionally(logger, 'connected');
                } else {
                    logger.warn(`not connected (expected ${VPN_STATIC_IP}, got ${vpnIp})`);
                }
                return of(hasVpn ? ConnectionStatus.UP : ConnectionStatus.DOWN);
            }),
            mergeMap(ipResult => {
                return from(PING.promise.probe(VPN_HOST_IP, {
                    timeout: 10,
                })).pipe(
                    switchMap(({alive: hasVpnHost}) => {
                        if (hasVpnHost) {
                            logConditionally(logger, 'host connected');
                        } else {
                            logger.warn(`host not connected`);
                        }
                        return of(hasVpnHost ? ConnectionStatus.UP : ConnectionStatus.DOWN);
                    }),
                    map(hostResult => [ipResult, hostResult].some(it => it == ConnectionStatus.DOWN) ? ConnectionStatus.DOWN : ConnectionStatus.UP)
                );
            }),
            catchError(err => {
                console.error('check failed', err);
                return of(ConnectionStatus.UNKNOWN);
            })
        );
    }

}
