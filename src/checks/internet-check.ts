import {catchError, switchMap, tap} from "rxjs/operators";
import {from, Observable, of} from "rxjs";
import * as PING from "ping";

import {ConnectionStatus, ConnectionType} from "../connections";
import {ICheck, logConditionally} from "./check";
import {ETH_STATIC_IP, PING_HOST} from "../constants";

const logger = new console.Console(process.stdout, process.stderr);
require('console-stamp')(logger, {
    metadata: '[' + ConnectionType[ConnectionType.INTERNET] + ']',
    colors: {
        metadata: require('chalk').blue.bold
    }
});

@ICheck.register
export class InternetCheck {

    public getCheckType() {
        return ConnectionType.INTERNET;
    }

    public getCheck(source: Observable<any>): Observable<ConnectionStatus> {
        logger.log(`starting ${ConnectionType[this.getCheckType()]} check with parameters: PING_HOST=${PING_HOST}`);
        return source.pipe(
            tap(() => logConditionally(logger, 'checking')),
            switchMap(() => {
                return from(PING.promise.probe(PING_HOST, {
                    timeout: 10,
                }));
            }),
            switchMap(({alive: hasInternet}) => {
                if (hasInternet) {
                    logConditionally(logger, 'connected');
                } else {
                    logger.warn(`not connected`);
                }
                return of(hasInternet ? ConnectionStatus.UP : ConnectionStatus.DOWN);
            }),
            catchError(err => {
                console.error('check failed', err);
                return of(ConnectionStatus.UNKNOWN);
            })
        );
    }

}
