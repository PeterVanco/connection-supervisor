import {Observable} from "rxjs";
import {ConnectionStatus, ConnectionType} from "../connections";
import {LOG_CHECKS} from "../constants";

export interface ICheck {

    getCheckType(): ConnectionType;

    getCheck(source: Observable<any>): Observable<ConnectionStatus>;

}

export namespace ICheck {
    type Constructor<T> = {
        new(...args: any[]): T;
        readonly prototype: T;
    }
    const implementations: Constructor<ICheck>[] = [];

    export function getImplementations(): Constructor<ICheck>[] {
        return implementations;
    }

    export function register<T extends Constructor<ICheck>>(ctor: T) {
        implementations.push(ctor);
        return ctor;
    }
}

export function logConditionally(logger, ...args) {
    if (LOG_CHECKS) {
        logger.log(args);
    }
}