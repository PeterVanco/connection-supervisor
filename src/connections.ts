import {BehaviorSubject} from "rxjs";

export enum ConnectionType {
    ETH,
    VPN,
    INTERNET
}

export enum ConnectionStatus {
    UNKNOWN,
    UP,
    DOWN
}

export class ConnectionStatusWrapper {
    public lastSeenUp: Date = new Date();
    private _observable = new BehaviorSubject(ConnectionStatus[ConnectionStatus.UNKNOWN]);

    public observableStatus() {
        return this._observable;
    }

    set status(value: ConnectionStatus) {
        if (value === ConnectionStatus.UP) {
            this.lastSeenUp = new Date();
        }
        this._observable.next(ConnectionStatus[value]);
    }

    public asObject() {
        return {
            lastSeenUp: this.lastSeenUp,
            status: this._observable.getValue()
        };
    }
}

export const connections = new Map<ConnectionType, ConnectionStatusWrapper>();

Object.keys(ConnectionType)
    .filter(key => !isNaN(Number(ConnectionType[key])))
    .forEach(key => connections.set(ConnectionType[key], new ConnectionStatusWrapper()))
