import * as os from 'os';

export class NetworkInterface {

    public constructor(private networkPrefix: string) {
    }

    public getIp(): string {
        let interfaces: { [index: string]: os.NetworkInterfaceInfo[] } = os.networkInterfaces();
        for (let name in interfaces) {
            const iface = interfaces[name].filter(ifc => ifc.family == 'IPv4')
                .map(ifc => ifc.address)
                .find(address => address.startsWith(this.networkPrefix));
            if (iface != null) {
                return iface;
            }
        }
        return null;
    }

    public matchesIp(ip: string) {
        return this.getIp() == ip;
    }

    public static printAll() {
        let interfaces: { [index: string]: os.NetworkInterfaceInfo[] } = os.networkInterfaces();
        for (let name in interfaces) {
            console.log(`${name}: ${JSON.stringify(interfaces[name], null, 2)}`);
        }
    }

}